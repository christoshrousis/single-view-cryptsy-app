//
//  MarketBuilder.m
//  Bitcoin to Litecoin
//
//  Created by Christos Hrousis on 18/03/2014.
//  Copyright (c) 2014 Think In Pixels. All rights reserved.
//

#import "MarketBuilder.h"
#import "Market.h"

@implementation MarketBuilder

+ (NSArray *)marketFromJSON:(NSData *)objectNotation error:(NSError **)error
{
    NSError *localError = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:objectNotation options:0 error:&localError];
    
    if (localError != nil) {
        *error = localError;
        return nil;
    }
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSDictionary *markets = [jsonDict valueForKey:@"return"];
    NSDictionary *ltc = [markets valueForKey:@"markets"];
    NSDictionary *ltcMarket = [ltc valueForKey:@"LTC"];
    
    NSString *primaryNameString = [ltcMarket valueForKey:@"primaryname"];
    NSString *primaryCodeString = [ltcMarket valueForKey:@"primarycode"];
    NSString *secondaryNameString = [ltcMarket valueForKey:@"secondaryname"];
    NSString *secondaryCodeString = [ltcMarket valueForKey:@"secondarycode"];
    NSString *lastTradePriceString = [ltcMarket valueForKey:@"lasttradeprice"];
    
    Market *market = [[Market alloc] init];
    market.primaryName = primaryNameString;
    market.primaryCode = primaryCodeString;
    market.secondaryName = secondaryNameString;
    market.secondaryCode = secondaryCodeString;
    market.lastTradePrice = lastTradePriceString;
    [result addObject:market];
    NSLog(@" Market Object: %@", market);
    
    return result;
}

@end
