//
//  CryptsyManager.m
//  Bitcoin to Litecoin
//
//  Created by Christos Hrousis on 18/03/2014.
//  Copyright (c) 2014 Think In Pixels. All rights reserved.
//

#import "CryptsyManager.h"
#import "MarketBuilder.h"
#import "CryptsyCommunicator.h"

@implementation CryptsyManager

- (void)fetchMarket
{
    [self.communicator getMarketData];
}

#pragma mark - CryptsyCommunicatorDelegate

- (void)receivedMarketJSON:(NSData *)objectNotation
{
    NSError *error = nil;
    NSArray *market = [MarketBuilder marketFromJSON:objectNotation error:&error];
    
    if (error != nil) {
        [self.delegate fetchingMarketFailedWithError:error];
        
    } else {
        [self.delegate didReceiveMarket:market];
    }
}

- (void)fetchingMarketFailedWithError:(NSError *)error
{
    [self.delegate fetchingMarketFailedWithError:error];
}
@end
