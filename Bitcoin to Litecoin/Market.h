//
//  Market.h
//  Bitcoin to Litecoin
//
//  Created by Christos Hrousis on 18/03/2014.
//  Copyright (c) 2014 Think In Pixels. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Market : NSObject
@property (strong, nonatomic) NSString *primaryName;
@property (strong, nonatomic) NSString *primaryCode;
@property (strong, nonatomic) NSString *secondaryName;
@property (strong, nonatomic) NSString *secondaryCode;

@property (strong, nonatomic) NSString *lastTradePrice;
@end
