//
//  ViewController.m
//  Bitcoin to Litecoin
//
//  Created by Christos Hrousis on 18/03/2014.
//  Copyright (c) 2014 Think In Pixels. All rights reserved.
//

#import "ViewController.h"

#import "Market.h"
#import "CryptsyManager.h"
#import "CryptsyCommunicator.h"

@interface ViewController () <CryptsyManagerDelegate> {
    NSArray *_marketArray;
    CryptsyManager *_manager;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _manager = [[CryptsyManager alloc] init];
    _manager.communicator = [[CryptsyCommunicator alloc] init];
    _manager.communicator.delegate = _manager;
    _manager.delegate = self;
    [_manager fetchMarket];
}

- (void)viewDidAppear
{
    [super viewDidAppear:true];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CryptsyManagerDelegate
- (void)didReceiveMarket:(NSArray *)market
{
    _marketArray = market;
    Market *tempMarket = [_marketArray objectAtIndex:0];
    
    [_primaryName performSelectorOnMainThread:@selector(setText:) withObject:tempMarket.primaryName waitUntilDone:YES];
    [_primaryCode performSelectorOnMainThread:@selector(setText:) withObject:tempMarket.primaryCode waitUntilDone:YES];
    [_secondaryName performSelectorOnMainThread:@selector(setText:) withObject:tempMarket.secondaryName waitUntilDone:YES];
    [_secondaryCode performSelectorOnMainThread:@selector(setText:) withObject:tempMarket.secondaryCode waitUntilDone:YES];
    
    [_lastTradePrice performSelectorOnMainThread:@selector(setText:) withObject:tempMarket.lastTradePrice waitUntilDone:YES];
    
    
}

- (void)fetchingMarketFailedWithError:(NSError *)error
{
    NSLog(@"Error %@; %@", error, [error localizedDescription]);
}


@end
