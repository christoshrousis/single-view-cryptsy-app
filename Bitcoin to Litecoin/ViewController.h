//
//  ViewController.h
//  Bitcoin to Litecoin
//
//  Created by Christos Hrousis on 18/03/2014.
//  Copyright (c) 2014 Think In Pixels. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, retain) IBOutlet UILabel *primaryName;
@property (nonatomic, retain) IBOutlet UILabel *primaryCode;
@property (nonatomic, retain) IBOutlet UILabel *secondaryName;
@property (nonatomic, retain) IBOutlet UILabel *secondaryCode;

@property (nonatomic, retain) IBOutlet UILabel *lastTradePrice;

@end
